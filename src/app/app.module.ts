import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PdfService } from './pdf.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { ExploreCategoriesComponent } from './explore-categories/explore-categories.component';
import { CategoriesCardComponent } from './categories-card/categories-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchbarComponent,
    ExploreCategoriesComponent,
    CategoriesCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
